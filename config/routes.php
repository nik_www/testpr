<?php

return [
    "/testpr" => "\Controller\IndexController/index",
    "/testpr/task/add" => "\Controller\TaskController/add",
    "/testpr/task/setstatus" => "\Controller\TaskController/setStatus",
    "/testpr/task/updatetext" => "\Controller\TaskController/updateText",
    "/testpr/auth" => "\Controller\AuthController/index",
    "/testpr/auth/login" => "\Controller\AuthController/login"
];
