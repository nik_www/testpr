<?php

namespace Model;

use Core\Request;

class AdminUser
{

    protected static $instance;
    protected $id;
    protected $name;

    public static function getInstance()
    {
        if (!isset(static::$instance)) {
            static::$instance = static::auth(Request::cookie('auser_hash'));
        }

        return static::$instance;
    }

    private function __construct($id, $name)
    {
        $this->id = $id;
        $this->name = $name;
    }

    public function getName()
    {
        return $this->name;
    }

    public static function login($name, $password)
    {
        $user_id = db()->query("SELECT id FROM AdminUsers WHERE name = '?s' AND password = '?s'", $name, md5($password))->getOne();

        if (!$user_id) {
            return false;
        }

        $hash = md5($user_id . $name . $password . time());
        db()->query("UPDATE AdminUsers SET hash = '?s' WHERE id = ?i", $hash, $user_id);

        setcookie('auser_hash', $hash, time() + 3600, '/');

        return true;
    }

    public static function auth($hash)
    {
        $result = db()->query("SELECT id, name FROM AdminUsers WHERE hash = '?s'", $hash)->fetch_assoc();

        if ($result && $result['id']) {
            return new static($result['id'], $result['name']);
        }

        return new static(0, '');
    }

    public function isAuthed()
    {
        return !empty($this->id);
    }

}
