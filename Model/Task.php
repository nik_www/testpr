<?php

namespace Model;

class Task
{

    private function __construct()
    {
        
    }

    public static function add($data)
    {
        if (empty($data['name']) || empty($data['email'])) {
            return false;
        }

        foreach ($data as &$value) {
            $value = protectInput($value);
        }

        db()->query("INSERT Tasks SET name = '?s', email = '?s', text = '?s'", $data['name'], $data['email'], $data['text']);

        return db()->getLastInsertId();
    }

    public static function updateText($task_id, $text)
    {
        db()->query("UPDATE Tasks SET text = '?s' WHERE id = ?i", protectInput($text), $task_id);
    }

    public static function setStatus($task_id, $status = true)
    {
        db()->query("UPDATE Tasks SET is_completed = ?i WHERE id = ?i", $status, $task_id);
    }

    public static function setFlagAdminEdited($task_id)
    {
        db()->query("UPDATE Tasks SET is_admin_edited = 1 WHERE id = ?i", $task_id);
    }

    public static function updateTextAndSetFlagAdminEdited($task_id, $text)
    {
        $oldText = db()->query("SELECT text FROM Tasks WHERE id = ?i", $task_id)->getOne();

        if ($oldText != $text) {
            static::updateText($task_id, $text);
            static::setFlagAdminEdited($task_id);
        }
    }

    public static function list(Pager $pager)
    {
        $orderby = Sort::getSqlOrderby();

        $query = "
            SELECT *
            FROM Tasks
            ORDER BY {$orderby}
            LIMIT {$pager->getStart()}, 3
        ";
        return db()->query($query)->fetch_assoc_array();
    }

    public static function getTotal()
    {
        return db()->query("SELECT COUNT(*) AS cnt FROM Tasks")->getOne();
    }

}
