<?php

namespace Model;

use Core\Request;

class Sort
{

    protected static $sort_fields = ['name', 'email', 'is_completed'];

    private function __construct()
    {
        
    }

    public static function getTplClass($name)
    {
        if (Request::get('orderby') == $name) {
            return Sort::getAsc() ? 'sort-up' : 'sort-down';
        }

        return '';
    }

    public static function getSqlOrderby()
    {
        if (!Request::get('orderby') || !in_array(Request::get('orderby'), static::$sort_fields)) {
            return 'id DESC';
        }

        return Request::get('orderby') . ' ' . (Sort::getAsc() ? 'ASC' : 'DESC');
    }

    public function getOrderby()
    {
        return Request::get('orderby');
    }

    public function getAsc()
    {
        return Request::get('asc');
    }

}
