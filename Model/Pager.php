<?php

namespace Model;

class Pager
{

    protected $current;
    protected $total;
    protected $start;

    public function __construct($current, $totalItems, $perPage)
    {
        $this->current = $current;
        $this->total = ceil($totalItems / $perPage);

        if ($this->current < 1) {
            $this->current = 1;
        }

        $this->start = ($this->current - 1) * $perPage;
    }

    public function render()
    {
        return renderTpl('pager', ['pager' => $this]);
    }

    public function getTotal()
    {
        return $this->total;
    }

    public function getCurrent()
    {
        return $this->current;
    }

    public function isEmpty()
    {
        return $this->total < 2;
    }

    public function getStart()
    {
        return $this->start;
    }

}
