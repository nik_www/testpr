<?php

namespace Core;

class Request
{

    private function __construct()
    {
        
    }

    public static function get($name)
    {
        return $_GET[$name] ?? null;
    }

    public static function post($name)
    {
        return $_POST[$name] ?? null;
    }

    public static function cookie($name)
    {
        return $_COOKIE[$name] ?? null;
    }

    public static function path()
    {
        return parse_url($_SERVER["REQUEST_URI"], PHP_URL_PATH);
    }

}
