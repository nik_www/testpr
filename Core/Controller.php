<?php

namespace Core;

class Controller
{

    public static function render($tmp, $vars = [])
    {
        echo renderTpl('header');
        echo renderTpl($tmp, $vars);
        echo renderTpl('footer');
    }

    public static function json($vars = [])
    {
        echo json_encode($vars);
    }

}
