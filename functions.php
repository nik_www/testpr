<?php

function db()
{
    static $db = null;

    if ($db === null) {
        $db = \Core\Mysql\Mysql::create(DB_HOST, DB_USER, DB_PASSWORD)->setDatabaseName(DB_DATABASE)->setCharset("utf8");
    }

    return $db;
}

function renderTpl($tmp, $vars = [])
{
    if (file_exists('templates/' . $tmp . '.tpl.php')) {
        ob_start();
        extract($vars);
        require 'templates/' . $tmp . '.tpl.php';
        return ob_get_clean();
    }
}

function protectInput($string)
{
    return htmlspecialchars(strip_tags($string), ENT_QUOTES);
}
