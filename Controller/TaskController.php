<?php

namespace Controller;

use Model\Task;
use Core\Request;
use Model\AdminUser;

class TaskController extends \Core\Controller
{

    public static function add()
    {
        $data = [
            'name' => Request::get('name'),
            'email' => Request::get('email'),
            'text' => Request::get('text')
        ];

        static::json(['status' => Task::add($data)]);
    }

    public function setStatus()
    {
        if (!AdminUser::getInstance()->isAuthed()) {
            static::json(['status' => false]);
            return;
        }

        Task::setStatus(Request::get('id'));

        static::json(['status' => true]);
    }

    public function updateText()
    {
        if (!AdminUser::getInstance()->isAuthed()) {
            static::json(['status' => false]);
            return;
        }

        Task::updateTextAndSetFlagAdminEdited(Request::get('id'), Request::get('text'));

        static::json(['status' => true]);
    }

}
