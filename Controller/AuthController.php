<?php

namespace Controller;

use Model\AdminUser;
use Core\Request;

class AuthController extends \Core\Controller
{

    public static function index()
    {
        static::render('auth', ['fail' => Request::get('fail')]);
    }

    public static function login()
    {
        if (empty(Request::post('name')) || empty(Request::post('password'))) {
            header('Location: /testpr/auth/?fail=empty');
            return;
        }

        if (AdminUser::login(Request::post('name'), Request::post('password'))) {
            header('Location: /testpr/');
            return;
        }

        header('Location: /testpr/auth/?fail=1');
    }

}
