<?php

namespace Controller;

use Model\Task;
use Model\Pager;
use Core\Request;

class IndexController extends \Core\Controller
{

    const TASKS_PER_PAGE = 3;

    public static function index()
    {
        $pager = new Pager(Request::get('page'), Task::getTotal(), Task::TASKS_PER_PAGE);

        static::render('tasks', [
            'tasks' => Task::list($pager),
            'pager' => $pager
        ]);
    }

}
