<!DOCTYPE HTML>
<html>
    <head>
        <link rel="stylesheet" href="/testpr/assets/bootstrap.min.css">
        <link rel="stylesheet" href="/testpr/assets/styles.css">
        
        <script src="/testpr/assets/jquery-3.5.1.min.js"></script>
        <script src="/testpr/assets/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
        
        <script src="/testpr/assets/scripts.js"></script>
    </head>
    <body>
        <header>
            <?php if (\Model\AdminUser::getInstance()->isAuthed()) { ?>
                Пользователь: <?= \Model\AdminUser::getInstance()->getName(); ?> <button type="button" href="/testpr/auth/" class="btn btn-secondary logout">Выйти</button>
            <?php } else { ?>
                <a href="/testpr/auth/" class="btn btn-info">Войти</a>
            <?php } ?>
        </header>