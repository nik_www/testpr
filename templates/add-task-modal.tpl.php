<div class="modal" id="add-task-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Добавление задачи</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form class="add-task">
                    <div class="form-group">
                        <label for="name-task">Имя</label>
                        <input type="text" name="name" id="name-task">
                    </div>
                    <div class="form-group">
                        <label for="email-task">Email</label>
                        <input type="text" name="email" id="email-task">
                    </div>
                    <div class="form-group">
                        <label for="text-task">Текст задачи</label>
                        <textarea name="text" id="text-task"></textarea>
                    </div>
                    <button type="submit" class="btn btn-info">Добавить</button>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="add-task-succes-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Добавление задачи</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Задача успешно добавлена</p>
                <button type="button" class="btn btn-success">Ok</button>
            </div>
        </div>
    </div>
</div>