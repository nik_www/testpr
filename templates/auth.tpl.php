
<form class="login" method="post" action="/testpr/auth/login">

    <?php if (Core\Request::get('fail')) {
        if (Core\Request::get('fail') == 'empty') {
            $error = 'Поля обязательны для заполнения';
        } else {
            $error = 'Неправильные данные для входа';
        } ?>
        <div class="alert alert-danger" role="alert"><?= $error ?></div>
    <?php } ?>

    <div class="form-group">
        <label for="name">Логин</label>
        <input type="text" name="name" id="name">
    </div>
    <div class="form-group">
        <label for="password">Пароль</label>
        <input type="password" name="password" id="password">
    </div>
    <button type="submit" class="btn btn-info">Войти</button>
</form>