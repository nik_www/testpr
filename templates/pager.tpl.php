<?php
if ($pager->isEmpty()) {
    return;
}
?>
<nav>
    <ul class="pagination">
        <?php for ($i = 1; $i <= $pager->getTotal(); $i++) { ?>
            <li class="page-item<?= $i == $pager->getCurrent() ? ' active' : '' ?>"><a class="page-link" href="#"><?= $i ?></a></li>
        <?php } ?>
    </ul>
</nav>