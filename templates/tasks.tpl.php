
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#add-task-modal">Добавить задачу</button>

<form class="list">
    <input type="hidden" name="page" value="<?= $pager->getCurrent() ?>">
    <input type="hidden" name="orderby" value="<?= \Model\Sort::getOrderby() ?>">
    <input type="hidden" name="asc" value="<?= \Model\Sort::getAsc() ?>">

    <table class="table">
        <thead>
        <tr>
            <th data-orderby="name" class="<?= \Model\Sort::getTplClass('name') ?>">Имя</th>
            <th data-orderby="email" class="<?= \Model\Sort::getTplClass('email') ?>">Email</th>
            <th>Текст</th>
            <th data-orderby="is_completed" class="<?= \Model\Sort::getTplClass('is_completed') ?>">Статус</th>
        </tr>
        </thead>
        <tbody>
            <?php foreach ($tasks as $task) { ?>
                <tr>
                    <td><?= $task['name'] ?></td>
                    <td><?= $task['email'] ?></td>
                    <td>
                        <?php if (!empty($task['is_admin_edited'])) { ?>
                            <span class="badge badge-warning">Отредактировано администратором</span>
                        <?php } ?>
                        <?php if (\Model\AdminUser::getInstance()->isAuthed()) { ?>
                            <div><textarea id="text<?= $task['id'] ?>"><?= $task['text'] ?></textarea></div>
                            <button type="button" class="btn btn-success btn-update" data-id="<?= $task['id'] ?>">Сохранить</button>
                        <?php } else { ?>
                            <?= $task['text'] ?>
                        <?php } ?>
                    </td>
                    <td>

                        <?php if (!empty($task['is_completed'])) { ?>
                            <span class="badge badge-success">Выполнено</span>
                        <?php } elseif (\Model\AdminUser::getInstance()->isAuthed()) { ?>
                            <button type="button" class="btn btn-success btn-status" data-id="<?= $task['id'] ?>">Проставить статус "выполненно"</button>
                        <?php } ?>
                    </td>
                </tr>
            <?php } ?>
        </tbody>
    </table>

    <?= $pager->render() ?>

</form>

<?= renderTpl('add-task-modal') ?>