<?php

require_once './autoload.php';
require_once './config/db.php';
$routes = require_once './config/routes.php';
require_once './functions.php';

\Core\RouterLite::addRoute($routes);
\Core\RouterLite::dispatch();