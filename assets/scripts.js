$(function () {
    $('.page-link').click(function () {
        $('[name="page"]').val($(this).text());
        $('form.list').submit();
        return false;
    });
    $('[data-orderby]').click(function () {
        $('[name="orderby"]').val($(this).data('orderby'));
        if ($(this).hasClass("sort-up")) {
            $('[name="asc"]').val("0");
        } else {
            $('[name="asc"]').val("1");
        }
        $('[name="page"]').val("1");
        $('form.list').submit();
        return false;
    });

    $('.add-task').submit(function () {
        var error = false;

        if (!$.trim($('.add-task [name="name"]').val())) {
            $('.add-task [name="name"]').parent().find('.text-danger').remove();
            $('.add-task [name="name"]').after('<small class="text-danger">Поле обязательно для заполнения</small>');
            error = true;
        }

        if (!$.trim($('.add-task [name="email"]').val())) {
            $('.add-task [name="email"]').parent().find('.text-danger').remove();
            $('.add-task [name="email"]').after('<small class="text-danger">Поле обязательно для заполнения</small>');
            error = true;
        } else if (!validateEmail($('.add-task [name="email"]').val())) {
            $('.add-task [name="email"]').parent().find('.text-danger').remove();
            $('.add-task [name="email"]').after('<small class="text-danger">Введите валидный email</small>');
            error = true;
        }

        if (error) {
            return false;
        }

        $.ajax({
            url: '/testpr/task/add/',
            data: $('.add-task').serialize(),
            dataType: 'json',
            success: function (json) {
                if (json.status) {
                    $('#add-task-modal').modal('hide');
                    $('#add-task-succes-modal').modal('show')
                            .find('.btn.btn-success').click(function () {
                        location.reload();
                    });
                }
            }
        });

        return false;
    });

    $('.logout').click(function () {
        setCookie('auser_hash', '', -1);
        location.reload();
    });
    
    $('.btn-status').click(function () {
        var id = $(this).data('id');

        $.ajax({
            url: '/testpr/task/setstatus/',
            data: 'id=' + id,
            dataType: 'json',
            success: function (json) {
                if (json.status) {
                    location.reload();
                } else {
                    location.href = '/testpr/auth/';
                }
            }
        });
    });
    
    $('.btn-update').click(function () {
        var id = $(this).data('id');
        var text = $('#text' + id).val();

        $.ajax({
            url: '/testpr/task/updatetext/',
            data: 'id=' + id + '&text=' + text,
            dataType: 'json',
            success: function (json) {
                if (json.status) {
                    location.reload();
                } else {
                    location.href = '/testpr/auth/';
                }
            }
        });
    });
});

function validateEmail(email) {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

function setCookie(cname, cvalue, exdays) {
  var d = new Date();
  d.setTime(d.getTime() + (exdays*24*60*60*1000));
  var expires = "expires="+ d.toUTCString();
  document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}